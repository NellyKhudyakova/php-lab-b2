<?php

const PLUS = '+';
const MINUS = '-';
const MULT = '*';
const DIV = '/';
const FLOAT_POINT = '.';
const PAREN_LEFT = '(';
const PAREN_RIGHT = ')';
const OPERATORS = [PLUS, MINUS, MULT, DIV];
const PARENTHESES = [PAREN_LEFT, PAREN_RIGHT];
const PRECEDENCE = array(PLUS => 1, MINUS => 1, MULT => 2, DIV => 2);

function numberBufferToNumber($numberBuffer) {
    $numArr = [''];
    $part = 0;
    $sign = 1;
    for ($i = 0; $i < strlen($numberBuffer); $i++) {
        if (ctype_digit($numberBuffer[$i])) {
            $numArr[$part] .= $numberBuffer[$i];
        } else if ($numberBuffer[$i] === MINUS) {
            if ($i !== 0) {
                throw new InvalidArgumentException('Error10');
            }
            $sign = -1;
        } else if ($numberBuffer[$i] === FLOAT_POINT) {
            $numArr[] = '';
            $part++;
        } else {
            throw new InvalidArgumentException('Error11');
        }
    }
    if (count($numArr) > 2) {
        throw new InvalidArgumentException('Error12');
    }
    for ($i = 0; $i < count($numArr); $i++) {
        if (strlen($numArr[$i]) === 0) {
            throw new InvalidArgumentException('Error13');
        }
    }

    $num1 = (float)$numArr[0];
    if (count($numArr) === 1) {
        if ($num1 === 0.0 && $sign === -1) {
            throw new InvalidArgumentException('Error14');
        }
    }
    if ((string)$num1 !== $numArr[0]) {
        throw new InvalidArgumentException('Error15');
    }
    if (count($numArr) === 2) {
        $num2 = (float)$numArr[1];
        if ($num1 === 0.0 && $num2 === 0.0) {
            throw new InvalidArgumentException('Error16');
        }
    }

    return (float)$numberBuffer;


//    if(!is_numeric($numberBuffer)) {
//        throw new InvalidArgumentException('Error10');
//    }
//    $num = (float)$numberBuffer;
//    if ((string)$num !== $numberBuffer) {
//        throw new InvalidArgumentException('Error11');
//    }
//    return $num;
}

function tokenize($expression)
{
    $exprLength = strlen($expression);
    $tokens = [];
    $numberBuffer = '';
    for($i = 0; $i < $exprLength; $i++) {
        if ($expression[$i] === MINUS && $i < strlen($expression) - 1 && ctype_digit($expression[$i + 1]) && ($i === 0 ||
            ((in_array($expression[$i - 1], OPERATORS) || $expression[$i - 1] === PAREN_LEFT)))) {
            if (strlen($numberBuffer) > 0) {
                throw new InvalidArgumentException('Error0');
            }
            $numberBuffer .= $expression[$i];
        } else if(ctype_digit($expression[$i]) || $expression[$i] === FLOAT_POINT) {
            $numberBuffer .= $expression[$i];
        } else if(strlen($numberBuffer) > 0) {
             $tokens[] = numberBufferToNumber($numberBuffer);
             $numberBuffer = '';
             $i--;
        } else if(in_array($expression[$i], PARENTHESES)) {
            $tokens[] = $expression[$i];
        } else if(in_array($expression[$i], OPERATORS)) {
            $tokens[] = $expression[$i];
        } else {
            throw new InvalidArgumentException('Error 1');
        }
    }
    if(strlen($numberBuffer) > 0) {
        $tokens[] = numberBufferToNumber($numberBuffer);
    }

    return $tokens;
}

function validateTokenizedExp($exp) {
    echo "\n";

    // Два оператора подряд
    for ($i = 1; $i < count($exp); $i++) {
        if (in_array($exp[$i], OPERATORS, true) && in_array($exp[$i - 1], OPERATORS, true)) {
            throw new InvalidArgumentException('Error 20');
        }
    }

    // Если первый или последний элемент операторы
    if ((count($exp) > 0 && in_array($exp[count($exp) - 1], OPERATORS, true)) ||
        (count($exp) > 1 && in_array($exp[0], OPERATORS, true) && $exp[1] !== PAREN_LEFT)) {
        throw new InvalidArgumentException('Error 22');
    }

    // Скобки
    $depth = 0;
    for ($i = 0; $i < count($exp); $i++) {
        if ($exp[$i] === PAREN_LEFT) {
            $depth++;
        } else if ($exp[$i] === PAREN_RIGHT) {
            $depth--;
            if ($depth < 0) {
                throw new InvalidArgumentException('Error 21');
            }
        }
    }
    if ($depth !== 0) {
        throw new InvalidArgumentException('Error 21');
    }

    // Еще одна проверка -0 )
    for ($i = 0; $i < count($exp); $i++) {
        if ($exp[$i] === -0) {
            throw new InvalidArgumentException('Error 23');
        }
    }


    return $exp;
}

function toRPN($exp) {
    $res = [];
    $opStack = [];
    for ($i = 0; $i < count($exp); $i++) {
        if (gettype($exp[$i]) === 'double') {
            $res[] = $exp[$i];
        } else if (in_array($exp[$i], OPERATORS, true)) {
            while (count($opStack) > 0 && in_array($opStack[count($opStack) - 1], OPERATORS, true) && PRECEDENCE[$exp[$i]] <= PRECEDENCE[$opStack[count($opStack) - 1]]) {
                $res[] = array_pop($opStack);
            }
            $opStack[] = $exp[$i];
        } else if ($exp[$i] === PAREN_LEFT) {
            $opStack[] = $exp[$i];
        } else if ($exp[$i] === PAREN_RIGHT) {
            while (count($opStack) && $opStack[count($opStack) - 1] !== PAREN_LEFT) {
                $res[] = array_pop($opStack);
            }
            if (count($opStack) === 0) {
                throw new InvalidArgumentException('Error 51');
            }
            array_pop($opStack);
        }
    }
    while (count($opStack) > 0) {
        $res[] = array_pop($opStack);
    }
    return $res;
}

function solveRPN($exp) {
    $stack = [];
    for ($i = 0; $i < count($exp); $i++) {
        if (in_array($exp[$i], OPERATORS, true)) {
            if ($exp[$i] === MINUS && count($stack) === 1) {
                $num = array_pop($stack);
                if (gettype($num) !== 'double') {
                    throw new InvalidArgumentException('Error 62');
                }
                $stack[] = -$num;

            } else {
                if (count($stack) < 2) {
                    throw new InvalidArgumentException('Error 61');
                }
                $num2 = array_pop($stack);
                $num1 = array_pop($stack);
                if (gettype($num1) !== 'double' || gettype($num2) !== 'double') {
                    throw new InvalidArgumentException('Error 62');
                }
                if ($exp[$i] === PLUS) {
                    $stack[] = $num1 + $num2;
                } else if ($exp[$i] === MINUS) {
                    $stack[] = $num1 - $num2;
                } else if ($exp[$i] === MULT) {
                    $stack[] = $num1 * $num2;
                } else if ($exp[$i] === DIV) {
                    if ($num2 === 0.0) {
                        throw new InvalidArgumentException('Error 63');
                    }
                    $stack[] = $num1 / $num2;
                }
            }
        } else {
            $stack[] = $exp[$i];
        }
    }

    if (count($stack) !== 1) {
        throw new InvalidArgumentException('Error 64');
    }

    return $stack[0];
}

function calc($exp) {
    $exp = preg_replace('/\s+/', '', $exp);

    echo "\nTOKENS\n";
    $exp = tokenize($exp);
    echo print_r($exp);

    echo "\nVALIDATION\n";
    validateTokenizedExp($exp);

    echo "\nRPN\n";
    $exp = toRPN($exp);
    echo print_r($exp);

    echo "\nCALC\n";
    $exp = solveRPN($exp);

    return $exp === 0.0 ? 0 : $exp;
}


const TESTS = array('-(-9)');

foreach (TESTS as $test) {
    echo (string)$test . "\n";
    echo calc($test);
}